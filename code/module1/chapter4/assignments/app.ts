// Getting inputs from first table
var t1:HTMLInputElement=<HTMLInputElement> document.getElementById("t1")
var t2:HTMLInputElement=<HTMLInputElement> document.getElementById("t2")
var t3:HTMLInputElement=<HTMLInputElement> document.getElementById("t3")

// Getting elements from second table
var t4:HTMLInputElement=<HTMLInputElement> document.getElementById("t4")
var t5:HTMLInputElement=<HTMLInputElement> document.getElementById("t5")
var t6:HTMLInputElement=<HTMLInputElement> document.getElementById("t6")

// getting elements from third table
var t7:HTMLInputElement=<HTMLInputElement> document.getElementById("t7")
var t8:HTMLInputElement=<HTMLInputElement> document.getElementById("t8")
var t9:HTMLInputElement=<HTMLInputElement> document.getElementById("t9")

// Getting elements from fourth table
var t10:HTMLInputElement=<HTMLInputElement> document.getElementById("t10")
var t11:HTMLInputElement=<HTMLInputElement> document.getElementById("t11")
var t12:HTMLInputElement=<HTMLInputElement> document.getElementById("t12")

function add(){
    // Adding the two inputs after converting them into numbers
    var c:number =parseFloat(t1.value)+parseFloat(t2.value);
    // Converting the sum back into string and displaying the result
    t3.value=c.toString();
}

function subtract(){
    // Subtracting second input from the first after converting them into numbers
    var c:number =parseFloat(t4.value)-parseFloat(t5.value);
    // Converting the difference back into string and displaying the result
    t6.value=c.toString();
}

function multiply(){
    // Multiplying the inputs after converting them into numbers
    var c:number =parseFloat(t7.value)*parseFloat(t8.value);
    // Converting the product back into string and displaying the result
    t9.value=c.toString();
}

function divide(){
    // Dividing the first input by the second after converting them into numbers
    var c:number =parseFloat(t10.value)/parseFloat(t11.value);
    // Converting the answer back into string and displaying the result
    t12.value=c.toString();
}