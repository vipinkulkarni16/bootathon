// Function to generate the dynamic table
function generate(){
    // getting inputs
    var table1:HTMLTableElement=<HTMLTableElement>document.getElementById("table1")
    var num:HTMLInputElement=<HTMLInputElement>document.getElementById("num");
    var count:number= +num.value

    // Deleting the existing rows
    while(table1.rows.length>1){
        table1.deleteRow(1);
    }

    // Generating the multiplication table
    for(var i=1;i<=count;i++){
        // Inserting a row and then cell in the row
        var row:HTMLTableRowElement=table1.insertRow();
        var cell:HTMLTableDataCellElement=row.insertCell();

        // Adding the proper strings into the cells
        var text=document.createElement("input")
        text.type="text";
        text.style.textAlign="center";
        text.style.fontSize="15px"
        text.value=count.toString();
        cell.appendChild(text)

        var text=document.createElement("input")
        text.type="text";
        text.style.textAlign="center"
        text.style.fontSize="15px"
        text.value="*"
        cell.appendChild(text)

        var text=document.createElement("input")
        text.type="text";
        text.style.textAlign="center"
        text.style.fontSize="15px"
        text.value=i.toString()
        cell.appendChild(text)

        var text=document.createElement("input")
        text.type="text";
        text.style.textAlign="center"
        text.style.fontSize="15px"
        text.value="="
        cell.appendChild(text)

        var text=document.createElement("input")
        text.type="text";
        text.style.textAlign="center"
        text.style.fontSize="15px"
        text.value=(count*i).toString()
        cell.appendChild(text)
    }
}
