function generate() {
    // getting inputs
    var table1 = document.getElementById("table1");
    var num = document.getElementById("num");
    var count = +num.value;
    // Deleting the existing rows
    while (table1.rows.length > 1) {
        table1.deleteRow(1);
    }
    // Generating the multiplication table
    for (var i = 1; i <= count; i++) {
        // Inserting a row and then cell in the row
        var row = table1.insertRow();
        var cell = row.insertCell();
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.fontSize = "15px";
        text.value = count.toString();
        cell.appendChild(text);
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.fontSize = "15px";
        text.value = "*";
        cell.appendChild(text);
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.fontSize = "15px";
        text.value = i.toString();
        cell.appendChild(text);
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.fontSize = "15px";
        text.value = "=";
        cell.appendChild(text);
        var text = document.createElement("input");
        text.type = "text";
        text.style.textAlign = "center";
        text.style.fontSize = "15px";
        text.value = (count * i).toString();
        cell.appendChild(text);
    }
}
//# sourceMappingURL=app.js.map