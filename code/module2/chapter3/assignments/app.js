// function to calculate area of triangle
function area(x1, y1, x2, y2, x3, y3) {
    // length of first side
    var a = (Math.sqrt(Math.pow((x2 - x1), 2) + Math.pow((y2 - y1), 2)));
    // length of second side
    var b = (Math.sqrt(Math.pow((x3 - x2), 2) + Math.pow((y3 - y2), 2)));
    // length of third side
    var c = (Math.sqrt(Math.pow((x3 - x1), 2) + Math.pow((y3 - y1), 2)));
    var s = (a + b + c) / 2;
    // Calculating area using Heron's Formula
    var area = (Math.sqrt(s * (s - a) * (s - b) * (s - c)));
    return area;
}
function check() {
    // Coorndinates of point A
    var x1 = document.getElementById("t11");
    var y1 = document.getElementById("t12");
    // Coordinates of point B
    var x2 = document.getElementById("t21");
    var y2 = document.getElementById("t22");
    // Coordinates of point C
    var x3 = document.getElementById("t31");
    var y3 = document.getElementById("t32");
    // Coordinates of point P
    var x4 = document.getElementById("p11");
    var y4 = document.getElementById("p12");
    var xa = parseFloat(x1.value);
    var ya = parseFloat(y1.value);
    var xb = parseFloat(x2.value);
    var yb = parseFloat(y2.value);
    var xc = parseFloat(x3.value);
    var yc = parseFloat(y3.value);
    var xp = parseFloat(x4.value);
    var yp = parseFloat(y4.value);
    // Checking for non-number inputs
    if (isNaN(xa) || isNaN(ya) || isNaN(xb) || isNaN(yb) || isNaN(xc) || isNaN(yc) || isNaN(xp) || isNaN(yp)) {
        alert("The inputs need to be numbers");
    }
    else {
        // Computing areas of the triangles
        var areaABC = area(xa, ya, xb, yb, xc, yc);
        var areaPAB = area(xp, yp, xa, ya, xb, yb);
        var areaPBC = area(xp, yp, xb, yb, xc, yc);
        var areaPCA = area(xp, yp, xc, yc, xa, ya);
        var sumarea = areaPAB + areaPBC + areaPCA;
        // Checking for the conditions
        if (areaABC == sumarea) {
            alert("The point is inside the triangle");
        }
        else {
            alert("The point is not inside the triangle");
        }
    }
}
//# sourceMappingURL=app.js.map