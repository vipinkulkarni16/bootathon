// function to calculate area of triangle
function area(x1,y1,x2,y2,x3,y3):number{
    // length of first side
    var a: number=(Math.sqrt(Math.pow((x2-x1),2)+Math.pow((y2-y1),2)));
    // length of second side
    var b: number=(Math.sqrt(Math.pow((x3-x2),2)+Math.pow((y3-y2),2)));
    // length of third side
    var c: number=(Math.sqrt(Math.pow((x3-x1),2)+Math.pow((y3-y1),2)));
    var s: number= (a+b+c)/2;
    // Calculating area using Heron's Formula
    var area: number=(Math.sqrt(s*(s-a)*(s-b)*(s-c)));
    return area;
}

function check(): void{
    // Coorndinates of point A
    var x1:HTMLInputElement=<HTMLInputElement>document.getElementById("t11");
    var y1:HTMLInputElement=<HTMLInputElement>document.getElementById("t12");
    // Coordinates of point B
    var x2:HTMLInputElement=<HTMLInputElement>document.getElementById("t21");
    var y2:HTMLInputElement=<HTMLInputElement>document.getElementById("t22");
    // Coordinates of point C
    var x3:HTMLInputElement=<HTMLInputElement>document.getElementById("t31");
    var y3:HTMLInputElement=<HTMLInputElement>document.getElementById("t32");
    // Coordinates of point P
    var x4:HTMLInputElement=<HTMLInputElement>document.getElementById("p11");
    var y4:HTMLInputElement=<HTMLInputElement>document.getElementById("p12");

    var xa:number= parseFloat(x1.value);
    var ya:number= parseFloat(y1.value);
    var xb:number= parseFloat(x2.value);
    var yb:number= parseFloat(y2.value);
    var xc:number= parseFloat(x3.value);
    var yc:number= parseFloat(y3.value);
    var xp:number= parseFloat(x4.value);
    var yp:number= parseFloat(y4.value);

    // Checking for non-number inputs
    if(isNaN(xa)||isNaN(ya)||isNaN(xb)||isNaN(yb)||isNaN(xc)||isNaN(yc)||isNaN(xp)||isNaN(yp)){
        alert("The inputs need to be numbers")
    }else{
        // Computing areas of the triangles
        var areaABC:number=area(xa,ya,xb,yb,xc,yc);
        var areaPAB:number=area(xp,yp,xa,ya,xb,yb);
        var areaPBC:number=area(xp,yp,xb,yb,xc,yc);
        var areaPCA:number=area(xp,yp,xc,yc,xa,ya);

        var sumarea:number= areaPAB +areaPBC + areaPCA;

        // Checking for the conditions
        if(areaABC==sumarea){
            alert("The point is inside the triangle");
        }else{
            alert("The point is not inside the triangle");
        }
    }    
}